defmodule Wampex.Router.MixProject do
  use Mix.Project

  def project do
    [
      app: :wampex_router,
      version: "0.2.0",
      elixir: "~> 1.17",
      start_permanent: Mix.env() == :prod,
      aliases: aliases(),
      deps: deps(),
      description: description(),
      package: package(),
      docs: [
        main: "readme",
        extras: ["README.md"],
        assets: "assets"
      ]
    ]
  end

  def description do
    "WAMP Router"
  end

  def package do
    [
      name: :wampex_router,
      files: ["lib", "priv", "mix.exs", "README*", "LICENSE*"],
      maintainers: ["Christopher Steven Coté"],
      licenses: ["MIT License"],
      links: %{
        "GitLab" => "https://gitlab.com/entropealabs/wampex_router"
      }
    ]
  end

  def application do
    [
      extra_applications: [:logger],
      env: [keylen: 32]
    ]
  end

  defp deps do
    [
      {:cluster_kv, "~> 0.1"},
      {:cors_plug, "~> 2.0"},
      {:credo, "~> 1.7", only: [:test], runtime: false},
      {:dialyxir, "~> 1.4", only: [:test], runtime: false},
      {:ex_doc, "~> 0.21", only: :dev, runtime: false},
      {:plug_cowboy, "~> 2.7"},
      {:states_language, "~> 0.4"},
      {:wampex, "~> 0.2"}
    ]
  end

  defp aliases do
    [
      all_tests: [
        "compile --force --warnings-as-errors",
        "credo --strict",
        "format --check-formatted",
        "dialyzer --halt-exit-status"
      ]
    ]
  end
end
