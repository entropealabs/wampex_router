# WAMPexRouter

A highly scalable WAMP Router implementing the Broker and Dealer roles. 

Utilizes distributed Erlang for a full mesh distributed cluster.

Under the hood it's using [ClusterKV](https://gitlab.com/entropealabs/cluster_kv) to provide an ephemeral distributed hash table key-value store.

ClusterKV utilizes [libring](https://github.com/bitwalker/libring) and [libcluster](https://github.com/bitwalker/libcluster) from [bitwalker](https://github.com/bitwalker/) to provide the hash table or "ring" and dynamic clustering, respectively.

This is used for session level things like subscriptions and registrations.

For persistant data such as users, realms, roles, device shadows, etc it utilizes CockroachDB. It uses Ecto and the postgrex driver, so it should be line compatible with a Postgresql server as well (not tested).

The Basic Profile is fully supported, as is "wampcra" authentication. In fact, you must authenticate in order to interact with the server.

There are several admin procedures available by default to create users and roles. `admin.create_user` and `admin.create_realm`

## Advanced Profile

In addition to authentication, [pattern-based subscriptions](https://wamp-proto.org/_static/gen/wamp_latest.html#pattern-based-subscriptions) are also supported.

Initial support for progressive call results for RPC is also available.

More coming soon!

