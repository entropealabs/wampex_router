defmodule Wampex.Router.Realms.Proxy do
  @moduledoc false
  use GenServer

  def start_link(name: name) do
    GenServer.start_link(__MODULE__, :ok, name: name)
  end

  def init(:ok) do
    {:ok, []}
  end

  def handle_info(messages, state) when is_list(messages) do
    Enum.each(messages, fn {e, to} -> send(to, e) end)
    {:noreply, state}
  end

  def handle_info({e, to}, state) do
    send(to, e)
    {:noreply, state}
  end

  def handle_call({:is_up, pid}, _from, s), do: {:reply, Process.alive?(pid), s}
end
